<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Проверка на антиплагиат</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/stylesait.css">
</head>
<body>
  <?php
     if($_COOKIE['user'] == ''):
  ?>
  <script>window.location.href = "/";</script>
  <?php
   else:
   ?>
   <div class="main">
   <div class="row">
   <div class="col-1-3">
     <div class='okno0'><a tabindex="7" href="/index3.php">Открыть список студентов</a></div>
   </div>
   <div class="col-1-3">
  <div class='okno1'>Проверка кода программы на плагиат</div>
  <div class='okno3'>
    <form action="work.php" method="post">
      <input tabindex="1" type="text" style="width: 90%; height: 30px; margin-left: 5%;" class="form-control" name="name1" id="name1" placeholder="Введите имя студента"> <br>
      <input tabindex="2" type="text" style="width: 90%; height: 30px;  margin-left: 5%;" class="form-control" name="name2" id="name2" placeholder="Введите фамилию студента"> <br>
      <input tabindex="3" type="text" style="width: 90%; height: 30px;  margin-left: 5%;" class="form-control" name="group" id="group" placeholder="Введите группу студента"> <br>
      <textarea tabindex="4" type="text" style="width: 90%; height: 450px;  margin-left: 5%;" class="form-control" name="kod" id="js-textarea" placeholder="Введите код программы"></textarea> <br>
      <input tabindex="5" type="file" style="margin-left: 5%;" id="js-file" accept=".txt,.css,.html,.cs">
      <button tabindex="6" style="margin-left: 1%;" class="btn btn-success" type="submit">Начать проверку</button>
    </form>
  </div>
  </div>
  <div class="col-1-3">
  <div class='okno'>Здравствуйте <?=$_COOKIE['user']?>. <a tabindex="8" href="/exit.php">Выход</a></div>
  </div>
  </div>
  </div>
  <script src="https://snipp.ru/cdn/jquery/2.1.1/jquery.min.js"></script>
  <script>
  $("#js-file").change(function(){
  var reader = new FileReader();
  reader.onload = function(e){
  $("#js-textarea").val(e.target.result);
  };
  reader.readAsText($("#js-file")[0].files[0], "UTF-8");
  });
  </script>
  <?php
  endif;
   ?>
</body>
</html>
