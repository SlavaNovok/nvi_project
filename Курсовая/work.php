<?php

$name1 = $_POST['name1'];
$name2 = $_POST['name2'];
$group = $_POST['group'];
$kod = $_POST['kod'];
if (mb_strlen(iconv("UTF-8", "windows-1251", $name1)) < 2 || mb_strlen(iconv("UTF-8", "windows-1251", $name1)) > 20) {
  echo "<style>
        .okno {
          width: 400px;
          height: 30px;
          text-align: center;
          padding: 15px;
          border: 3px solid #000000;
          border-radius: 15px;
          color: #000000;
          position:absolute;
          top:50%;
          left:50%;
          margin:-200px 0 0 -200px;
        }
  </style>";
  echo "<div class='okno'><a href='/exit1.php'>Недопустимая длина имени студента</a></div>";
  exit();
} else if (mb_strlen(iconv("UTF-8", "windows-1251", $name2)) < 2 || mb_strlen(iconv("UTF-8", "windows-1251", $name2)) > 30) {
    echo "<style>
          .okno {
            width: 400px;
            height: 30px;
            text-align: center;
            padding: 15px;
            border: 3px solid #000000;
            border-radius: 15px;
            color: #000000;
            position:absolute;
            top:50%;
            left:50%;
            margin:-200px 0 0 -200px;
          }
    </style>";
    echo "<div class='okno'><a href='/exit1.php'>Недопустимая длина фамилии студента</a></div>";
    exit();
} else if (mb_strlen(iconv("UTF-8", "windows-1251", $group)) < 2 || mb_strlen(iconv("UTF-8", "windows-1251", $group)) > 7) {
      echo "<style>
            .okno {
              width: 400px;
              height: 30px;
              text-align: center;
              padding: 15px;
              border: 3px solid #000000;
              border-radius: 15px;
              color: #000000;
              position:absolute;
              top:50%;
              left:50%;
              margin:-200px 0 0 -200px;
            }
      </style>";
      echo "<div class='okno'><a href='/exit1.php'>Недопустимая длина группы студента</a></div>";
      exit();
} else if (mb_strlen($kod) < 3 ) {
        echo "<style>
              .okno {
                width: 400px;
                height: 30px;
                text-align: center;
                padding: 15px;
                border: 3px solid #000000;
                border-radius: 15px;
                color: #000000;
                position:absolute;
                top:50%;
                left:50%;
                margin:-200px 0 0 -200px;
              }
        </style>";
        echo "<div class='okno'><a href='/exit1.php'>Недопустимая длина кода программы</a></div>";
        exit();
}

$kod=stripslashes($kod);
$wskod=iconv("UTF-8", "windows-1251", $kod);
$h=mb_strlen($wskod);
$arr1 = str_split($wskod);
$i=0;
$h=count($arr1);
//Запись в string изначального текста;
$string = implode($arr1);
$wsstring = iconv("windows-1251", "UTF-8", $string);
$string1=nl2br($wsstring);
//удаление переносов строки и запись в string текста
for ($i=0; $i<=$h;$i++) {
  if (($arr1[$i]=="\r") || ($arr1[$i]=="\n")) {
    unset($arr1[$i]);
  }
}
$h=count($arr1);
$arr2 = array_values($arr1);
$string2 = implode($arr2);
$wsstring2 = iconv("windows-1251", "UTF-8", $string2);
//удаление пробелов и запись в string текста
for ($i=0; $i<=$h;$i++) {
  if ($arr2[$i]==" ") {
    unset($arr2[$i]);
  }
}
$h=count($arr2);
$arr3 = array_values($arr2);
$string3 = implode($arr3);
$wsstring3 = iconv("windows-1251", "UTF-8", $string3);
//Удаление знаков и вывод текста
$string4 = preg_replace('/[^ a-zа-яё\d]/ui', '',$wsstring3 );
$wsstring4=iconv("UTF-8", "windows-1251", $string4);
$h=mb_strlen($wsstring4);
//Удаление цифр и вывод текста
$string5 = preg_replace('/\d/', '', $string4);
$wsstring5 = iconv("UTF-8", "windows-1251", $string5);
$h=mb_strlen($wsstring5);
//Подсчет количества строк в таблице
$mysql = new mysqli('localhost','root','','sait');
$result = $mysql->query("SELECT COUNT(*) FROM `students`");
$kolstr=$result->fetch_assoc();
$kolstr=implode($kolstr);
//Что делать если строк в таблице нет
$lstring1=addslashes($string1);
$lstring2=addslashes($wsstring2);
$lstring3=addslashes($wsstring3);
$lstring4=addslashes($string4);
$lstring5=addslashes($string5);
if ($kolstr == 0) {
  $mysql->query("INSERT INTO `students` (`name`, `secondname`, `group`) VALUES('$name1','$name2','$group')");
  $result = $mysql->query("SELECT `id` FROM `students` WHERE `name`='$name1' AND `secondname`='$name2' AND `group` = '$group'");
  $id=$result->fetch_assoc();
  $id= implode($id);
  $mysql->query("INSERT INTO `text` (`id_student`, `kod1`, `kod2`, `kod3`, `kod4`, `kod5`) VALUES('$id','$lstring1','$lstring2', '$lstring3','$lstring4','$lstring5')");
  $result = $mysql->query("SELECT `id` FROM `text` WHERE `id_student`='$id'");
  $id_txt=$result->fetch_assoc();
  $id_txt = implode($id_txt);
  $id_txt2 = 0;
  $result = 100;
  $mysql->query("INSERT INTO `results` (`id_text`, `id_text2`, `result`) VALUES('$id_txt','$id_txt2','$result')");
  $id_std=1;
  $id_results=1;
  $kd='';
  $mysql->query("INSERT INTO `kods` (`id_student`, `id_text1`, `id_text2`, `id_results`, `text1`, `result`, `text2`) VALUES('$id_std','$id_txt','$id_txt2','$id_results','$lstring1','$result','$kd')");

} else {
  $result = $mysql->query("SELECT `id` FROM `students` WHERE `name`='$name1' AND `secondname`='$name2' AND `group` = '$group'");
  $id=$result->fetch_assoc();
  if ($id == "") {
     $mysql->query("INSERT INTO `students` (`name`, `secondname`, `group`) VALUES('$name1','$name2','$group')");
     $result = $mysql->query("SELECT `id` FROM `students` WHERE `name`='$name1' AND `secondname`='$name2' AND `group` = '$group'");
     $id=$result->fetch_assoc();
  }
  $id= implode($id);
  $pr0 = array();
  $pr = array();
  $pr1 = array();
  $pr2 = array();
  $pr3 = array();
  $pr4 = array();
  $i=0;
  $result = $mysql->query("SELECT * FROM `text`");
  while ($row=mysqli_fetch_assoc($result)) {
    $pr0[$i]=$row['id'];
    $pr[$i]=iconv("UTF-8", "windows-1251", $row['kod1']);
    $pr[$i]=stripslashes($pr[$i]);
    $pr1[$i]=iconv("UTF-8", "windows-1251", $row['kod2']);
    $pr1[$i]=stripslashes($pr1[$i]);
    $pr2[$i]=iconv("UTF-8", "windows-1251", $row['kod3']);
    $pr2[$i]=stripslashes($pr2[$i]);
    $pr3[$i]=iconv("UTF-8", "windows-1251", $row['kod4']);
    $pr3[$i]=stripslashes($pr3[$i]);
    $pr4[$i]=iconv("UTF-8", "windows-1251", $row['kod5']);
    $pr4[$i]=stripslashes($pr4[$i]);
    $i++;
  }
  $kolstr=$i;
  function br2nl($string)
  {
    return str_replace("<br />", "", $string);
  }
  $rez1 = array();
  $rez2 = array();
  $rez3 = array();
  $rez4 = array();
  $rez5 = array();
  for ($i=0;$i<$kolstr;$i++) {
     $pr[$i]=br2nl($pr[$i]);
     //Проверка оригинальности у изначального текста
     $dl1=mb_strlen($string);
     $dl2=mb_strlen($pr[$i]);
     if ($dl1 > $dl2) {
       $rz=$dl1-$dl2;
       $da=0;
       $arr1 = $pr[$i];
       for ($j=2;$j<$dl2;$j=$j+3) {
         if ($string[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rz=(int)(($rz+($dl2%3))/3);
       $rez1[$i]=($da+$rz)/$dl1;
     } else if ($dl2 >= $dl1) {
       $da=0;
       $arr1 = $pr[$i];
       $wsarr1=(iconv("windows-1251", "UTF-8", $arr1));
       for ($j=2;$j<$dl1;$j=$j+3) {
         if ($string[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rez1[$i]=($da)/$dl1;
     }
     //проверка оригинальности у текста без переходов на новую строку
     $dl1=mb_strlen($string2);
     $dl2=mb_strlen($pr1[$i]);
     if ($dl1 > $dl2) {
       $rz=$dl1-$dl2;
       $da=0;
       $arr1 = $pr1[$i];
       for ($j=2;$j<$dl2;$j=$j+3) {
         if ($string2[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rz=(int)(($rz+($dl2%3))/3);
       $rez2[$i]=($da+$rz)/$dl1;
     } else if ($dl2 >= $dl1) {
       $da=0;
       $arr1 = $pr1[$i];
       $wsarr1=(iconv("windows-1251", "UTF-8", $arr1));
       for ($j=2;$j<$dl1;$j=$j+3) {
         if ($string2[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rez2[$i]=($da)/$dl1;
     }
     //проверка оригинальности у текста без пробелов
     $dl1=mb_strlen($string3);
     $dl2=mb_strlen($pr2[$i]);
     if ($dl1 > $dl2) {
       $rz=$dl1-$dl2;
       $da=0;
       $arr1 = $pr2[$i];
       for ($j=2;$j<$dl2;$j=$j+3) {
         if ($string3[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rz=(int)(($rz+($dl2%3))/3);
       $rez3[$i]=($da+$rz)/$dl1;
     } else if ($dl2 >= $dl1) {
       $da=0;
       $arr1 = $pr2[$i];
       $wsarr1=(iconv("windows-1251", "UTF-8", $arr1));
       for ($j=2;$j<$dl1;$j=$j+3) {
         if ($string3[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rez3[$i]=($da)/$dl1;
     }
     //проверка оригинальности без знаков
     $dl1=mb_strlen($wsstring4);
     $dl2=mb_strlen($pr3[$i]);
     if ($dl1 > $dl2) {
       $rz=$dl1-$dl2;
       $da=0;
       $arr1 = $pr3[$i];
       for ($j=2;$j<$dl2;$j=$j+3) {
         if ($wsstring4[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rz=(int)(($rz+($dl2%3))/3);
       $rez4[$i]=($da+$rz)/$dl1;
     } else if ($dl2 >= $dl1) {
       $da=0;
       $arr1 = $pr3[$i];
       $wsarr1=(iconv("windows-1251", "UTF-8", $arr1));
       for ($j=2;$j<$dl1;$j=$j+3) {
         if ($wsstring4[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rez4[$i]=($da)/$dl1;
     }
     //проверка без цифр
     $dl1=mb_strlen($wsstring5);
     $dl2=mb_strlen($pr4[$i]);
     if ($dl1 > $dl2) {
       $rz=$dl1-$dl2;
       $da=0;
       $arr1 = $pr4[$i];
       for ($j=2;$j<$dl2;$j=$j+3) {
         if ($wsstring5[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rz=(int)(($rz+($dl2%3))/3);
       $rez5[$i]=($da+$rz)/$dl1;
     } else if ($dl2 >= $dl1) {
       $da=0;
       $arr1 = $pr4[$i];
       $wsarr1=(iconv("windows-1251", "UTF-8", $arr1));
       for ($j=2;$j<$dl1;$j=$j+3) {
         if ($wsstring5[$j]!=$arr1[$j]) {
            $da=$da+1;
         }
       }
       $dl1=(int)($dl1/3);
       $rez5[$i]=($da)/$dl1;
     }
  }
  $rez=array();
  $rezultat=101;
  //нахождение результата проверки
  for ($i=0;$i<$kolstr;$i++) {
      $rez[$i]=($rez1[$i]+$rez2[$i]+$rez3[$i]+$rez4[$i]+$rez5[$i])/5;
      $rez[$i]=round($rez[$i],2)*100;
      if ($rezultat > $rez[$i]) {
        $rezultat=$rez[$i];
      }
  }
  //Поиск текстов с наибольшим совпадением
  $id_t=array();
  $j=0;
  for ($i=0;$i<$kolstr;$i++) {
     if ($rezultat==$rez[$i]) {
       $id_t[$j]=$i;
       $j=$j+1;
     }
  }
  $j=$j-1;
  $kolstr=$j;
  $t1=array();
  for ($i=0;$i<=$kolstr;$i++) {
    $j=$id_t[$i];
    $l=$pr0[$j];
    $t1[$i]=$l;
  }
  //Добавление записи в таблицу текстов
  $mysql->query("INSERT INTO `text` (`id_student`, `kod1`, `kod2`, `kod3`, `kod4`, `kod5`) VALUES('$id','$lstring1','$lstring2', '$lstring3','$lstring4','$lstring5')");
  $id_sstd=$id;


  //поиск id в таблице текстов
  $result = $mysql->query("SELECT `id` FROM `text` ORDER BY `id` DESC LIMIT 1");
  $id=$result->fetch_assoc();
  $id= implode($id);
  for ($i=0;$i<=$kolstr;$i++) {
        $mysql->query("INSERT INTO `results` (`id_text`, `id_text2`, `result`) VALUES('$id','$t1[$i]','$rezultat')");
        $id_tt1=$id;
        $mysql->close();
        $mysql = new mysqli('localhost','root','','sait');
        $result = $mysql->query("SELECT `id` FROM `results` WHERE `id_text`='$id'");
        $id_results=$result->fetch_assoc();
        $id_results= implode($id_results);
        $result = $mysql->query("SELECT `kod1` FROM `text` WHERE `id`='$t1[$i]'");
        $kd=$result->fetch_assoc();
        $kd= implode($kd);
        $kd=stripslashes($kd);
        $kd=addslashes($kd);
        $mysql->query("INSERT INTO `kods` (`id_student`, `id_text1`, `id_text2`, `id_results`, `text1`, `result`, `text2`) VALUES('$id_sstd','$id_tt1','$t1[$i]','$id_results','$lstring1','$rezultat','$kd')");

  }


  $result = $mysql->query("SELECT `kod1` FROM `text` WHERE `id`='$t1[$kolstr]'");
  $text=$result->fetch_assoc();
  $text= implode($text);
  $text=stripslashes($text);
  $result = $mysql->query("SELECT `id_student` FROM `text` WHERE `id`='$t1[$kolstr]'");
  $id=$result->fetch_assoc();
  $id= implode($id);
  $result = $mysql->query("SELECT `name` FROM `students` WHERE `id`='$id'");
  $name3=$result->fetch_assoc();
  $name3= implode($name3);
  $result = $mysql->query("SELECT `secondname` FROM `students` WHERE `id`='$id'");
  $name4=$result->fetch_assoc();
  $name4= implode($name4);
  $result = $mysql->query("SELECT `group` FROM `students` WHERE `id`='$id'");
  $group1=$result->fetch_assoc();
  $group1= implode($group1);

}

$mysql->close();

require_once('index2.php');

?>
